package com.analizadores;

import java_cup.runtime.Symbol;

%%
%cupsym Simb
%cup
%class Scanner
%public
%line
%char
%column
%full

%%
"("                         {return new Symbol(Simb.par_a, yyline, yycolumn, yytext());}
")"                         {return new Symbol(Simb.par_c, yyline, yycolumn, yytext());}
","                         {return new Symbol(Simb.coma, yyline, yycolumn, yytext());}
"()"                        {return new Symbol(Simb.vacio, yyline, yycolumn, yytext());}
[A-ZñÑ]([a-z]|[A-Z]|ñ|Ñ)+   {return new Symbol(Simb.nombre, yyline, yycolumn, yytext());}
[ \t\r\f\n]+                {/**ignorar*/}
.                           {
                                System.err.println("Problema no se reconoce: " + yytext());
                            }