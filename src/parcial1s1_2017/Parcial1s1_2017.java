/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package parcial1s1_2017;
import com.analizadores.*;
import java.io.BufferedReader;
import java.io.StringReader;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.Stack;
/**
 *
 * @author wxjoy
 */
public class Parcial1s1_2017 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Stack<String> stj = new Stack<>();
        String entrada = "Armando@(Carlos(Dinora(),David( Iris())), Carmen(),Cristobal(Fabiola(German(),Gabriela())))";
        //String entrada = "Mateo()";
        Scanner sc = new Scanner(new BufferedReader(new StringReader(entrada)));
        var ps = new Parser(sc);
        try {
            ps.parse();
        } catch (Exception ex) {
            Logger.getLogger(Parcial1s1_2017.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
}
